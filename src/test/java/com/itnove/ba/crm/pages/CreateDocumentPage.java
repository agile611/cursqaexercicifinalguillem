package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateDocumentPage {

    private WebDriver driver;

    @FindBy(id = "filename_file")
    public WebElement filename;

    @FindBy(xpath = "(.//*[@id='SAVE'])[1]")
    public WebElement saveButton;

    public void browseFile(String name){
        filename.sendKeys(name);
    }

    public void saveDocument(){
        saveButton.click();
    }

    public CreateDocumentPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
